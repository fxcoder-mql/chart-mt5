# Список изменений в Chart

## 6.0 (2023-08-01)

- только MT5

## 5.2 ()

- исправлено: неверная работа в 5 (fxcoder-mql/chart#59)

## 5.1 ()

- исправлено: показывается только последний бар, если таймфрейм ниже текущего (fxcoder-mql/chart#2)
- исправлено: неверное определение числа знаков, если необходимо значение 8 (fxcoder-mql/chart#3)
- улучшено: полная перерисовка чужого таймфрема только на новом баре (было - каждый тик)

## 5.0 (2019-04-14)

- поддержка MT5
- не работал и был не нужен параметр BackColor, удалён, теперь цвет фона всегда берётся как цвет фона графика
- удалён параметр BodyWidth, теперь ширина тела свечи всегда вычисляется автоматически
- возможность установить цвета основного графика (включено по умолчанию)
- автоматическое определение числа знаков, актуально для реверсов символов с большими значениями
- убрано умножение на 100 при логарифмировании

## 4.1 (2015-12-30)

- удалены устаревшие функции
- более оперативное обновление ширины тела свечи
- более оперативное реагирование на изменение цвета фона
- изменён тип параметра Timeframe, но в числовом представлении он остался прежним, поэтому сохранилась совместимость со старыми шаблонами

## 4.0 (2014-06-19)

- отрицательные значения без смещения
- Shift смещает вправо
