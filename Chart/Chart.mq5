/*
Copyright 2023 FXcoder

This file is part of Chart.

Chart is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Chart is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with Chart. If not, see
http://www.gnu.org/licenses/.
*/

#property copyright "Chart 6.0. © FXcoder"
#property link      "https://fxcoder.blogspot.com"

#property indicator_separate_window
#property indicator_buffers 6 // The first buffer is for value, others are for candle drawing
#property indicator_plots 2


#include "Chart-include/message.mqh"
#include "Chart-include/plot/calcplot.mqh"
#include "Chart-include/plot/colorcandlesplot.mqh"
#include "Chart-include/conv.mqh"
#include "Chart-include/ind.mqh"
#include "Chart-include/indicator.mqh"
#include "Chart-include/series.mqh"
#include "Chart-include/symbol.mqh"
#include "Chart-include/tf.mqh"


input group "SOURCE"
input string             ChartSym          = "";             // Symbol
input bool               Reverse           = false;          // Reverse
input ENUM_TIMEFRAMES    ChartPer          = PERIOD_CURRENT; // Timeframe

input group "VISUAL"
input color              BullColor         = clrNONE;        // Bull Color (None = main chart colors)
input color              BearColor         = clrNONE;        // Bear Color (None = main chart colors)

input group "SERVICE"
input ENUM_APPLIED_PRICE ValueAppliedPrice = PRICE_CLOSE;    // Value Applied Price
input int                MaxBars           = 0;              // Maximum Bars
input int                Shift             = 0;              // Shift
input bool               LogScale          = false;          // Logarithmic Scale


CCalcPlot plot_value_;
CColorCandlesPlot plot_;

CSymbol sym_(ChartSym);
CSeries src_ser_(ChartSym, ChartPer);
CSeries sync_ser_(_Symbol, ChartPer);

bool  is_initialized_ = false;

color bg_color_ = clrNONE;
color bull_color_ = BullColor;
color bear_color_ = BearColor;

string MSG_SYMBOL_ERR   = _mql.program_name() + ": Error in symbol";


void OnInit()
{
	plot_value_.init(0, 0).label("Value");
	plot_value_.shift(Shift);

	plot_.init(plot_value_);
	plot_.set_two_color(BullColor, BearColor);
	plot_.shift(Shift);

	_indicator.short_name(_mql.program_name() + ":" + (Reverse ? "1/" : "") + sym_.name() + "," + _tf.to_string(ChartPer) + (LogScale ? ",log" : ""));

	is_initialized_ = sym_.exists();

	if (is_initialized_)
	{
		_indicator.digits(_Digits);
	}

	if (!is_initialized_)
		_imsg.show(MSG_SYMBOL_ERR, clrRed);
}

int OnCalculate(const int rates_total, const int prev_calculated, const datetime &time[], const double &open[], const double &high[], const double &low[], const double &close[], const long &tick_volume[], const long &volume[], const int &spread[])
{
	if (!is_initialized_)
		return 0;

	ArraySetAsSeries(time, true);

	// Обновить внешний вид
	update_colors();

	/*
	Полная перерисовка должна происходить в случаях:
		1. появился новый бар у источника
		2. появился новый бар у текущего графика
		3. изменилось число доступных баров у источника
		4. изменилось число доступных баров у текущего графика

	3 и 4 заменяют 1 и 2, можно остановиться только на них. Однако получение числа баров - медленная операция, возможно
		придётся использовать только 1 и 2.

	Это не самый лучший алгоритм. Было бы лучше не только проверять наличие новых баров, но и пересчитывать только
		вновь появившиеся.
	*/

	// обновить всё при любом из условий:
	//   - таймфрейм или символ чужие
	//   - есть новые бары
	bool full_update = prev_calculated == 0;
	bool is_current_symbol = _sym.is_current(ChartSym);
	bool is_current_tf = _tf.is_current(ChartPer);
	bool is_current_series = is_current_tf && is_current_symbol;
	bool sync_by_chart_symbol = is_current_symbol || is_current_tf;

	if (!is_current_series)
	{
		// Проверить, изменилось ли число баров или время открытия последнего бара для текущего графика и источника.
		static int chart_bars = 0;
		static int src_bars   = 0;

		bool is_chart_new_bars = chart_bars != (chart_bars = _series.bars_count());
		bool is_src_new_bars   = src_bars   != (src_bars   = src_ser_.bars_count());

		static datetime chart_last_bar_time = 0;
		static datetime src_last_bar_time   = 0;

		bool is_chart_new_bar = chart_last_bar_time != (chart_last_bar_time = _series.time(0));
		bool is_src_new_bar   = src_last_bar_time   != (src_last_bar_time   = src_ser_.time(0));

		bool is_new_bars = is_chart_new_bars || is_src_new_bars || is_chart_new_bar || is_src_new_bar;

		full_update = full_update || is_new_bars;
	}

	const int bars_to_calc = _ind.get_bars_to_calculate(rates_total, full_update ? 0 : prev_calculated, 1, MaxBars, 0);

	plot_.empty(full_update ? -1 : bars_to_calc);
	plot_value_.empty(full_update ? -1 : bars_to_calc);
	int calculated_bars = rates_total - bars_to_calc;


	for (int i = 0; i < bars_to_calc; i++)
	{
		int pbar = rates_total - 1 - i;

		// Синхронизация всегда происходит по символу и тф индикатора, даже если это приводит
		//   к разрывам или пропуску значимых баров.
		datetime dt = sync_ser_.time(i);
		int src_bar = src_ser_.bar_shift(dt, true);

		// Если бара для данного времени нет, то не отображаем ничего, но в значение пишем прошлый бар
		if (src_bar < 0)
		{
			if (pbar - 1 >= 0)
				plot_value_.buffer[pbar] = plot_value_.buffer[pbar - 1];

			continue;
		}

		double o, h, l, c, value;
		if (!get_price(src_bar, o, h, l, c, value))
			break;

		// Установка значений в буферы линий
		if (LogScale)
		{
			o = log(o);
			h = log(h);
			l = log(l);
			c = log(c);
			value = log(value);
		}

		plot_.set_candle(pbar, o, h, l, c);
		plot_value_.buffer[pbar] = value;
		calculated_bars++;
	}

	int draw_begin = MaxBars > 0 ? (rates_total - MaxBars) : (rates_total - calculated_bars);
	plot_.draw_begin(draw_begin);
	plot_value_.draw_begin(draw_begin);

	if (full_update)
		update_digits();

	return calculated_bars;
}

void OnChartEvent(const int id, const long& lparam, const double& dparam, const string& sparam)
{
	if (id == CHARTEVENT_CHART_CHANGE)
	{
		if (update_colors())
			ChartRedraw();
	}
}

void OnDeinit(const int reason)
{
	_imsg.hide();
}

void update_digits()
{
	double max = -DBL_MAX;
	double min = DBL_MAX;

	const int bar_count = plot_value_.size();
	const int scan_count = _math.min(1000, MaxBars, bar_count);

	for (int i = 0; i < scan_count; i++)
	{
		double value = plot_value_.buffer[bar_count - 1 - i];

		if (value == EMPTY_VALUE)
			continue;

		if (value > max)
			max = value;

		if (value < min)
			min = value;
	}

	if (max > min)
	{
		const int max_digits = 8;
		const int min_digits = 0;

		const int new_digits = _math.clamp(
			int(ceil(log10(1.0 / (max - min)))) + 2,
			min_digits, max_digits);

		if (new_digits != _indicator.digits())
			_indicator.digits(new_digits);
	}
}

bool update_colors()
{
	bool is_changing = false;

	// Цвет фона
	color new_bg_color = _chart.color_background();

	if (new_bg_color != bg_color_)
	{
		bg_color_ = new_bg_color;
		is_changing = true;
	}


	// Цвета свечи

	if (_color.is_none(BullColor))
	{
		color new_bull_color = _chart.color_chart_up();

		if (new_bull_color != bull_color_)
		{
			bull_color_ = new_bull_color;
			is_changing = true;
		}
	}

	if (_color.is_none(BearColor))
	{
		color new_bear_color = _chart.color_chart_down();

		if (new_bear_color != bear_color_)
		{
			bear_color_ = new_bear_color;
			is_changing = true;
		}
	}

	if (is_changing)
		plot_.set_two_color(bull_color_, bear_color_);


	return is_changing;
}

bool get_price(int bar, double &open, double &high, double &low, double &close, double &value)
{
	double o = src_ser_.open(bar);
	double h = src_ser_.high(bar);
	double l = src_ser_.low(bar);
	double c = src_ser_.close(bar);

	if (o <= 0 || h <= 0 || l <= 0 || c <= 0)
		return false;

	open  = Reverse ? 1.0 / o : o;
	high  = Reverse ? 1.0 / l : h;
	low   = Reverse ? 1.0 / h : l;
	close = Reverse ? 1.0 / c : c;
	value = _conv.ohlc_to_price(open, high, low, close, ValueAppliedPrice);

	return true;
}
