/*
Copyright 2023 FXcoder

This file is part of Chart.

Chart is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Chart is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with Chart. If not, see
http://www.gnu.org/licenses/.
*/

// Conversion Functions. © FXcoder

class CConvertUtil
{
public:

	/*
	Преобразовать массив целых чисел в сокращённый список.
	suffix добавляется только если значений больше 2, и они разные.
	Примеры:
		[1, 2, 3, 4, 5, 6, 7]  => "1..7/7" + suffux
		[3, 5, 8, 13, 21]      => "3..21/5" + suffux
	*/
	static string range_to_string(const int &range[], string suffix)
	{
		int sorted[];
		ArrayCopy(sorted, range);
		ArraySort(sorted);

		const int count = ArraySize(sorted);

		if (count <= 0)
			return "";

		if (count == 1)
			return (string)sorted[0];

		if (count == 2)
			return (string)sorted[0] + "," + (string)sorted[1];

		// all are equal
		if (sorted[0] == sorted[count - 1])
			return (string)sorted[0] + "/" + (string)count;

		return (string)sorted[0] + ".." + (string)sorted[count - 1] + "/" + (string)count + suffix;
	}
	static double ohlc_to_price(double open, double high, double low, double close, ENUM_APPLIED_PRICE applied_price)
	{
		switch (applied_price)
		{
			case PRICE_CLOSE:    return close;
			case PRICE_OPEN:     return open;
			case PRICE_HIGH:     return high;
			case PRICE_LOW:      return low;
			case PRICE_MEDIAN:   return (high + low) / 2.0;
			case PRICE_TYPICAL:  return (high + low + close) / 3.0;
			case PRICE_WEIGHTED: return (high + low + close + close) / 4.0;
		}

		// этот исход возможен только при явной ошибке в applied_price, либо если в новых версий появятся новые типы
		return EMPTY_VALUE;
	}
} _conv;
