/*
Copyright 2023 FXcoder

This file is part of Chart.

Chart is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Chart is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with Chart. If not, see
http://www.gnu.org/licenses/.
*/

// Класс цветного свечного графика: DRAW_COLOR_CANDLES. © FXcoder

#include "plot.mqh"

class CColorCandlesPlot: public CPlot
{
public:

	double buffer_open[];
	double buffer_high[];
	double buffer_low[];
	double buffer_close[];

protected:

	double buffer_color[];
	color bull_color_;
	color bear_color_;
	color back_color_;

public:

	color bull_color() const { return bull_color_; }
	color bear_color() const { return bear_color_; }
	color back_color() const { return back_color_; }

	// Конструктор по умолчанию (не инициализировать)
	void CColorCandlesPlot():
		CPlot(),
		bull_color_(clrNONE),
		bear_color_(clrNONE),
		back_color_(clrNONE)
	{
	}

	void CColorCandlesPlot(int plot_index_first, int buffer_index_first):
		CPlot(),
		bull_color_(clrNONE),
		bear_color_(clrNONE),
		back_color_(clrNONE)
	{
		init(plot_index_first, buffer_index_first);
	}

	void CColorCandlesPlot(const CPlot &prev):
		CPlot(),
		bull_color_(clrNONE),
		bear_color_(clrNONE),
		back_color_(clrNONE)
	{
		init(prev.plot_index_next(), prev.buffer_index_next());
	}

	virtual CColorCandlesPlot *init(int plot_index_first, int buffer_index_first) override
	{
		init_properties();

		plot_index_first_ = plot_index_first;
		plot_index_count_ = 1;
		buffer_index_first_ = buffer_index_first;
		buffer_index_count_ = 5;

		SetIndexBuffer(buffer_index_first_ + 0, buffer_open,  INDICATOR_DATA);
		SetIndexBuffer(buffer_index_first_ + 1, buffer_high,  INDICATOR_DATA);
		SetIndexBuffer(buffer_index_first_ + 2, buffer_low,   INDICATOR_DATA);
		SetIndexBuffer(buffer_index_first_ + 3, buffer_close, INDICATOR_DATA);
		SetIndexBuffer(buffer_index_first_ + 4, buffer_color, INDICATOR_COLOR_INDEX);

		draw_type(DRAW_COLOR_CANDLES); // после SetIndexBuffer
		return &this;
	}

	virtual CColorCandlesPlot *init(const CPlot &prev) override
	{
		return init(prev.plot_index_next(), prev.buffer_index_next());
	}

	// Размер буфера
	virtual int size() const override
	{
		// любой буфер подойдёт
		return ArraySize(buffer_open);
	}


	// bars - число последних баров для очистки, -1 = все
	virtual CColorCandlesPlot *empty(int bars = -1) override
	{
		double empty_value = empty_value();

		if (bars < 0)
		{
			ArrayInitialize(buffer_open,  empty_value);
			ArrayInitialize(buffer_high,  empty_value);
			ArrayInitialize(buffer_low,   empty_value);
			ArrayInitialize(buffer_close, empty_value);
			ArrayInitialize(buffer_color, 0);
		}
		else
		{
			// empty может быть вызван до инициализации буферов и первого тика
			int size = size();

			if (bars > size)
				bars = size;

			int offset = size - bars;

			ArrayFill(buffer_open,  offset, bars, empty_value);
			ArrayFill(buffer_high,  offset, bars, empty_value);
			ArrayFill(buffer_low,   offset, bars, empty_value);
			ArrayFill(buffer_close, offset, bars, empty_value);
			ArrayFill(buffer_color, offset, bars, 0);
		}

		return &this;
	}


	virtual CColorCandlesPlot *empty_bar(int bar) override
	{
		double empty_value = empty_value();

		buffer_open  [bar] = empty_value;
		buffer_high  [bar] = empty_value;
		buffer_low   [bar] = empty_value;
		buffer_close [bar] = empty_value;
		buffer_color [bar] = 0;

		return &this;
	}


	// Переопределения

	// префикс + типовое имя буфера
	virtual CColorCandlesPlot *label(string prefix) override
	{
		CPlot::label(prefix + "Open;" + prefix + "High;" + prefix + "Low;" + prefix + "Close");
		return &this;
	}

	// true, если значения были изменены
	bool set_two_color(color bull_color, color bear_color, bool auto_colors = false)
	{
		return set_colors(bull_color, bear_color, auto_colors);
	}

	// true, если значения были изменены
	bool set_colors(color bull_color, color bear_color, bool auto_colors = false)
	{
		const color back_color = _chart.color_background();

		// Automatic colors
		//
		// bull_color  bear_color  meaning
		// none        none        both color are chart's colors
		// none        valid       bull color is chart's color
		// valid       none        bear_color is bull_color
		// valid       valid       both are not automatic
		//
		// If bull_color == bear_color then separate the colors by brightness.

		bear_color = _color.validate(bear_color, bull_color);

		if (auto_colors)
		{
			bull_color = _color.validate(bull_color, _chart.color_chart_up());
			bear_color = _color.validate(bear_color, _chart.color_chart_down());

			if (bear_color == bull_color)
			{
				bull_color = _color.mix(back_color, bull_color, 5.0 / 4.0);
				bear_color = _color.mix(back_color, bear_color, 4.0 / 5.0);
			}
		}

		if ((bull_color == bull_color_) && (bear_color == bear_color_) && (back_color == back_color_))
			return false;

		bull_color_ = bull_color;
		bear_color_ = bear_color;
		back_color_ = back_color;

		color_indexes(2);
		line_color(0, bull_color);
		line_color(1, bear_color);

		return true;
	}

	bool update_width()
	{
		return true;
	}

	void set_candle(int bar, double open, double high, double low, double close)
	{
		buffer_open  [bar] = open;
		buffer_high  [bar] = high;
		buffer_low   [bar] = low;
		buffer_close [bar] = close;
		buffer_color [bar] = close < open ? 1 : 0;
	}

	void set_candle(int bar, const MqlRates &rate)
	{
		set_candle(bar, rate.open, rate.high, rate.low, rate.close);
	}

	// Добавить значение к указанному бару
	void set_value(int bar, double value, bool reset = false)
	{
		double empty_value = empty_value();

		if (reset || (value == empty_value) || !is_valid_bar(bar))
		{
			set_candle(bar, value, value, value, value);
			return;
		}

		buffer_close[bar] = value;

		if (value > buffer_high[bar])
			buffer_high[bar] = value;
		else if (value < buffer_low[bar])
			buffer_low[bar] = value;

		buffer_color[bar] = value >= buffer_open[bar] ? 0 : 1;
	}

	void set_last(double last, bool reset)
	{
		set_value(0, last, reset);
	}

	bool is_valid_bar(int bar)
	{
		double empty_value = empty_value();

		return
			(buffer_open [bar] != empty_value) &&
			(buffer_high [bar] != empty_value) &&
			(buffer_low  [bar] != empty_value) &&
			(buffer_close[bar] != empty_value);
	}
};
